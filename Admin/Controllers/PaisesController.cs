﻿using System.Web;
using System.Web.Mvc;
using Data.Entites;

namespace Admin.Controllers
{
    public class PaisesController : Controller
    {
        // GET: Paises
        public ActionResult Index(int? page, string search = "")
        {
            ViewBag.Page = page;
            ViewBag.Search = search;
            return View(Application.PaisService.GetPaises(page,search));
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Pais pais, HttpPostedFileBase Imagem)
        {
            if (ModelState.IsValid)
            {
                if (Application.PaisService.NewPais(pais, Imagem)) return RedirectToAction("Index");
            }

            return View(pais);
        }

        public ActionResult Details(int id)
        {
            return View(Application.PaisService.GetPais(id));
        }



    }
}