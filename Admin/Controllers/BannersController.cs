﻿
using System.Web;
using System.Web.Mvc;
using Data.Entites;

namespace Admin.Controllers
{
    public class BannersController : Controller
    {
        // GET: Banners
        public ActionResult Index(int? page,string search = "")
        {
            ViewBag.Page = page;
            ViewBag.Search = search;
            return View(Application.BannerService.GetBanners(page,search));
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Banner banner, HttpPostedFileBase Imagem)
        {
            if (ModelState.IsValid)
            {
                if (Application.BannerService.NewBanner(banner, Imagem)) return RedirectToAction("Index");
            }
            return View(banner);
        }

        public ActionResult Details(int id)
        {
            return View(Application.BannerService.GetBanner(id));
        }

        public ActionResult Edit(int id)
        {
            return View(Application.BannerService.GetBanner(id));
        }

        [HttpPost]
        public ActionResult Edit(Banner banner, HttpPostedFileBase img)
        {
            if (ModelState.IsValid)
            {
                if (Application.BannerService.ChangeBanner(banner, img)) return RedirectToAction("Index");
            }
            return View(banner);
        }

        public ActionResult Delete(int id)
        {
            Application.BannerService.DeleteBanner(id);
            return RedirectToAction("Index");
        }
    }
}