﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using Data.DAL;
using Data.Entites;
using PagedList;

namespace Admin.Application
{
    public static class BannerService
    {
        public static IPagedList<Banner> GetBanners(int? page, string search = "")
        {
            try
            {
                using (var db = new Context())
                {
                    var list = db.Banners.Where(b => !b.Excluido);
                    var pagedList = from obj in list select obj;
                    if (!string.IsNullOrEmpty(search))
                    {
                        search = search.ToLower();
                        pagedList =
                            pagedList.Where(
                                b =>
                                    b.Texto.ToLower().Contains(search) || b.Link.ToLower().Contains(search) ||
                                    b.Titulo.ToLower().Contains(search));
                    }

                    pagedList = pagedList.OrderByDescending(b => b.DataCadastro);

                    return pagedList.ToPagedList(page ?? 1, 15);
                }
            }
            catch (Exception)
            {
                return new List<Banner>().ToPagedList(page ?? 1, 15);
            }
        }

        

        public static bool NewBanner(Banner banner, HttpPostedFileBase img)
        {
            try
            {
                using (var db = new Context())
                {
                    var imageName = img.FileName;
                    if (!string.IsNullOrEmpty(imageName))
                    {
                        string[] extensions = { ".jpg", ".png", ".bmp" };

                        var ext = System.IO.Path.GetExtension(img.FileName);

                        if (!extensions.Contains(ext))
                        {
                            return false;
                        }
                        img.SaveAs(HostingEnvironment.MapPath("~/conteudo/banners/") + imageName);

                    }
                    banner.Imagem = img.FileName;
                    db.Banners.Add(banner);
                    db.SaveChanges();
                    db.Dispose();

                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public static Banner GetBanner(int id)
        {
            return ServiceBase<Banner>.FindById(id);
        }

        public static bool ChangeBanner(Banner banner, HttpPostedFileBase img)
        {
            try
            {
                using (var db = new Context())
                {
                    if (img != null)
                    {
                        var imageName = img.FileName;
                        string[] extensions = { ".jpg", ".png", ".bmp" };
                        var ext = System.IO.Path.GetExtension(img.FileName);

                        if (!extensions.Contains(ext))
                        {
                            return false;
                        }
                        System.IO.File.Delete(HostingEnvironment.MapPath("~/conteudo/banner/") + banner.Imagem);
                        img.SaveAs(HostingEnvironment.MapPath("~/conteudo/banner/") + imageName);
                        banner.Imagem = img.FileName;
                    }
                    ServiceBase<Banner>.Edit(banner);
                    db.SaveChanges();
                    db.Dispose();
                   
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static void DeleteBanner(int id)
        {
            try
            {
                using (var db = new Context())
                {
                    var banner = ServiceBase<Banner>.FindById(id);
                    banner.Excluir();
                    ServiceBase<Banner>.Edit(banner);
                    
                }
            }
            catch (Exception)
            {
               
            }
        }
    }
}
