﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using Data.DAL;
using Data.Entites;
using PagedList;

namespace Admin.Application
{
    public static class PaisService
    {
        public static IPagedList<Pais> GetPaises(int? page, string search = "")
        {
            try
            {
                using (var db = new Context())
                {
                    var list = db.Paises.Where(p => !p.Excluido);
                    var pagedList = from obj in list select obj;

                    if (!string.IsNullOrEmpty(search))
                    {
                        search = search.ToLower();
                        pagedList = pagedList.Where(p => p.Nome.ToLower().Contains(search));
                    }

                    pagedList = pagedList.OrderByDescending(p => p.DataCadastro);

                    return pagedList.ToPagedList(page ?? 1, 15);
                }
            }
            catch (Exception)
            {
                return new List<Pais>().ToPagedList(page ?? 1, 15);
            }
        }

        public static Pais GetPais(int id)
        {
            return ServiceBase<Pais>.FindById(id);
        }

        public static bool NewPais(Pais pais, HttpPostedFileBase img)
        {
            try
            {
                using (var db = new Context())
                {
                    var imageName = img.FileName;
                    if (!string.IsNullOrEmpty(imageName))
                    {
                        string[] extensions = { ".jpg", ".png", ".bmp" };

                        var ext = System.IO.Path.GetExtension(img.FileName);

                        if (!extensions.Contains(ext))
                        {
                            return false;
                        }
                        img.SaveAs(HostingEnvironment.MapPath("~/conteudo/paises/") + imageName);

                    }
                    pais.Imagem = img.FileName;
                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }


    }
}
