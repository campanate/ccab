﻿

using System;
using System.Data.Entity;
using Data.DAL;

namespace Admin.Application
{
    public static class ServiceBase<T> where T : class
    {
        public static T FindById(int id)
        {
            try
            {
                using (var db = new Context())
                {
                    return db.Set<T>().Find(id);
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static bool Add(T t)
        {
            try
            {
                using (var db = new Context())
                {
                    db.Set<T>().Add(t);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool Edit(T t)
        {
            try
            {
                using (var db = new Context())
                {
                    db.Entry(t).State = EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
                
            }
            catch (Exception)
            {
                return false;
            }
        }

        
    }
}
