﻿

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using GPF1.Domain.Entities;

namespace Data.Entites
{
    [Table("Banners")]
    public class Banner : EntityBase
    {
        [StringLength(100,ErrorMessage = "Título: Limite de 100 caracteres.")]
        [Required(ErrorMessage = "Título: Campo requerido.")]
        [DisplayName("Título")]
        public string Titulo { get; set; }

        [StringLength(100,ErrorMessage = "Imagem: Limite de 100 caracteres.")]
        [Required(ErrorMessage = "Imagem: Campo requerido.")]
        [DisplayName("Imagem")]
        public string Imagem { get; set; }

        [StringLength(300, ErrorMessage = "Texto: Limite de 300 caracteres.")]
        [Required(ErrorMessage = "Texto: Campo requerido.")]
        [DisplayName("Texto")]
        public string Texto { get; set; }

        [StringLength(100, ErrorMessage = "Link: Limite de 100 caracteres.")]
        [Required(ErrorMessage = "Link: Campo requerido.")]
        [DisplayName("Link")]
        public string Link { get; set; }
    }
}
