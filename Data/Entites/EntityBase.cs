﻿using System;
using System.ComponentModel;

namespace GPF1.Domain.Entities
{
    public abstract class EntityBase
    {
        public int Id { get; set; }
        public DateTime? DataAtualizacao { get; set; }

        [DisplayName("Data de Cadastro")]
        public DateTime DataCadastro{ get; set; }
        public Boolean Liberado { get; set; }
        public Boolean Excluido { get; private set; }

        public void Excluir()
        {
            Excluido = true;
            Liberado = false;
        }
    }
}
