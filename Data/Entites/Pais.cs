﻿

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using GPF1.Domain.Entities;

namespace Data.Entites
{
    public class Pais : EntityBase
    {
        [StringLength(100,ErrorMessage = "Nome: Limite de 100 caracteres.")]
        [Required(ErrorMessage = "Nome: Campo requerido.")]
        [DisplayName("Nome")]
        public string Nome { get; set; }

        [StringLength(100, ErrorMessage = "Imagem: Limite de 100 caracteres.")]
        [Required(ErrorMessage = "Imagem: Campo requerido.")]
        [DisplayName("Imagem")]
        public string Imagem { get; set; }
    }
}
